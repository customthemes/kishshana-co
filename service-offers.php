<?php
/**
 * This is the Offers page template for the Pretty Creative Theme.
 *
 * @author Katrina M
 * @package Pretty Creative
 * @subpackage Customizations
 */

/*
Template Name: Offers
*/

//* Add custom body class to the head
add_filter( 'body_class', 'prettycreative_add_body_class' );
function prettycreative_add_body_class( $classes ) {

   $classes[] = 'prettycreative-offers';
   return $classes;
   
}

//* Force full width content layout
add_filter( 'genesis_site_layout', '__genesis_return_full_width_content' );

//* Remove breadcrumbs
remove_action( 'genesis_before_loop', 'genesis_do_breadcrumbs' );

//* Remove site footer widgets
remove_action( 'genesis_before_footer', 'genesis_footer_widget_areas' );


//* Run the Genesis loop
genesis();
